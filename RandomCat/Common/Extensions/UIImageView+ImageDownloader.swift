//
//  UIImageView+ImageDownloader.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImage(fromURL url: URL, activityIndicator: UIActivityIndicatorView) {
        let imageDownloader = ImageDownloaderController()
        
        imageDownloader.downloadImage(withURL: url, completion: { [weak self] (image) in
            DispatchQueue.main.async {
                self?.image = image
                activityIndicator.stopAnimating()
            }
        }, failure: {() -> Void in
            DispatchQueue.main.async {
                activityIndicator.stopAnimating()
            }
        })
    }
}

//
//  ImageDownloaderController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

class ImageDownloaderController: NSObject, URLSessionDelegate {
    
    private let cache = NSCache<AnyObject, AnyObject>()
    
    func downloadImage(withURL url: URL, completion: @escaping (_ image: UIImage) -> Void, failure: (() -> Void)? = nil) {
        if let image = retrieveImageFromCache(name: url.absoluteString) {
            completion(image)
        } else {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
            let dataTask = session.dataTask(with: url, completionHandler: {[weak self] (data, response, error) in
                if let downloadedData = data {
                    if let image = UIImage(data: downloadedData) {
                        completion(image)
                        
                        if let urlString = response?.url?.absoluteString {
                            self?.saveImageToCache(image: image, name: urlString)
                        }
                    } else {
                        failure?()
                    }
                }
            })
            
            dataTask.resume()
        }
    }
    
    
    // MARK: Private methods
    
    private func retrieveImageFromCache(name: String) -> UIImage? {
        if let image = cache.object(forKey: name as AnyObject) as? UIImage {
            return image
        }
        
        return nil
    }
    
    private func saveImageToCache(image: UIImage, name: String) {
        cache.setObject(image, forKey: name as AnyObject)
    }
}

//
//  RemoteController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import Foundation

let userId = "lester1234"
let apiKey = "MjQwNzkw"

typealias RemoteControllerCompletionHandler = (_ data: Data, _ response: URLResponse) -> Void
typealias RemoteControllerFailureHandler = (_ error: Error) -> Void

class RemoteController {
    
    let baseURL = "https://thecatapi.com/api/images"
    
    func performNetworkRequest(_ request: URLRequest, completionHandler: @escaping RemoteControllerCompletionHandler, failureHandler: @escaping RemoteControllerFailureHandler) {
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let err = error {
                failureHandler(err)
            }
            
            if let responseData = data, let requestResponse = response {
                completionHandler(responseData, requestResponse)
            }
        })
        
        task.resume()
    }
}

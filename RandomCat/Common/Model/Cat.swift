//
//  Cat.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import SWXMLHash
import UIKit

struct XMLKey {
    
    static let response = "response"
    static let data = "data"
    static let images = "images"
    static let image = "image"
    static let catId = "id"
    static let sourceURLString = "source_url"
    static let urlString = "url"
}

struct Cat: XMLIndexerDeserializable {
    
    let catId: String
    let sourceURLString: String
    let urlString: String
    
    var isFavorite = false
    
    var url: URL? {
        return URL(string: urlString)
    }
    
    struct Keys {
        static let catId = "catId"
        static let sourceURLString = "sourceURLString"
        static let urlString = "urlString"
        static let isFavorite = "isFavorite"
    }
    
    static func deserialize(_ node: XMLIndexer) throws -> Cat {
        return try Cat (
            catId: node[XMLKey.catId].value(),
            sourceURLString: node[XMLKey.sourceURLString].value(),
            urlString: node[XMLKey.urlString].value(),
            isFavorite: false
        )
    }
    
    mutating func markAsFavorite() {
        self.isFavorite = true
    }
    
    mutating func unfavorite() {
        self.isFavorite = false
    }
}

extension Cat {
    @objc(CodedCat)class Coding: NSObject, NSCoding {
        
        let cat: Cat?
        
        init(cat: Cat) {
            self.cat = cat
            super.init()
        }
        
        required init?(coder aDecoder: NSCoder) {
            guard let catId = aDecoder.decodeObject(forKey: Cat.Keys.catId) as? String,
            let sourceURLString = aDecoder.decodeObject(forKey: Cat.Keys.sourceURLString) as? String,
            let urlString = aDecoder.decodeObject(forKey: Cat.Keys.urlString) as? String else {
                return nil
            }
            
            let isFavorite = aDecoder.decodeBool(forKey: Cat.Keys.isFavorite)
            
            cat = Cat(catId: catId, sourceURLString: sourceURLString, urlString: urlString, isFavorite: isFavorite)
            
            super.init()
        }
        
        public func encode(with aCoder: NSCoder) {
            guard  let cat = cat else {
                return
            }
            
            aCoder.encode(cat.catId, forKey: Cat.Keys.catId)
            aCoder.encode(cat.sourceURLString, forKey: Cat.Keys.sourceURLString)
            aCoder.encode(cat.urlString, forKey: Cat.Keys.urlString)
            aCoder.encode(cat.isFavorite, forKey: Cat.Keys.isFavorite)
        }
    }
}

extension Cat: Encodable {
    
    var encoded: Decodable? {
        return Cat.Coding(cat: self)
    }
}

extension Cat.Coding: Decodable {
    var decoded: Encodable? {
        return self.cat
    }
}

// MARK: Protocols

protocol Encodable {
    var encoded: Decodable? { get }
}

protocol Decodable {
    var decoded: Encodable? { get }
}


extension Sequence where Iterator.Element: Encodable {
    var encoded: [Decodable] {
        return self.filter({ $0.encoded != nil }).map({ $0.encoded! })
    }
}


extension Sequence where Iterator.Element: Decodable {
    var decoded: [Encodable] {
        return self.filter({ $0.decoded != nil }).map({ $0.decoded! })
    }
}

//
//  FavoriteCatTableViewCell.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

class FavoriteCatTableViewCell: UITableViewCell {

    @IBOutlet weak var catImageView: UIImageView!
    @IBOutlet weak var catIdLabel: UILabel!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView! {
        didSet {
            indicatorView.activityIndicatorViewStyle = .gray
            indicatorView.hidesWhenStopped = true
            indicatorView.startAnimating()
        }
    }
    
    private var cat: Cat!
    
    static let identifier = "Cell"
    
    func configure(cat: Cat) {
        self.cat = cat
        
        catIdLabel.text = cat.catId
        
        guard let url = cat.url else {
            return
        }
        
        catImageView.loadImage(fromURL: url, activityIndicator: indicatorView)
    }
}

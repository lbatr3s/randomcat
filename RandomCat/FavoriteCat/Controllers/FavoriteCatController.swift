//
//  FavoriteCatController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import Foundation

protocol FavoriteCatControllerDelegate: class {
    func didFavoriteCat(_ cat: Cat)
    func didFailFavoritingCat()
    func didUnfavoriteCat(_ cat: Cat)
    func didFailUnfavoritingCat()
}

class FavoriteCatController {
    
    private let remoteController: FavoriteCatRemoteController
    
    weak var delegate: FavoriteCatControllerDelegate?
    
    init() {
        remoteController = FavoriteCatRemoteController()
        remoteController.delegate = self
    }
    
    func markAsFavorite(_ cat: Cat) {
        remoteController.markAsFavorite(cat)
    }
    
    func unfavorite(_ cat: Cat) {
        remoteController.unfavorite(cat)
    }
}


// MARK: FavoriteCatRemoteController delegate methods

extension FavoriteCatController: FavoriteCatRemoteControllerDelegate {
    
    func didFavoriteCat(_ cat: Cat) {
        delegate?.didFavoriteCat(cat)
    }
    
    func didFailFavoritingCat() {
        delegate?.didFailFavoritingCat()
    }
    
    func didUnfavoriteCat(_ cat: Cat) {
        delegate?.didUnfavoriteCat(cat)
    }
    
    func didFailUnfavoritingCat() {
        delegate?.didFailUnfavoritingCat()
    }
}

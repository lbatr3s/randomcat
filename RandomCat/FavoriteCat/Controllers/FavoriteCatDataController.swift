//
//  FavoriteCatDataController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import Foundation

class FavoriteCatDataController {
    
    private let filename = "favoriteCats"
    private var cats: [Cat]!
    
    static let sharedInstance = FavoriteCatDataController()
    
    private init() {
        if let storedCats = unarchiveObjectFromFile(file: filename) as? [Cat] {
            cats = storedCats
        } else {
            cats = []
        }
    }
    
    func saveCat(_ cat: Cat) {
        cats.append(cat)
        archiveObjectToFile(object: cats, file: filename)
    }
    
    func removeCat(_ cat: Cat) {
        guard let index = cats.index(where: {(anotherCat) -> Bool in
            return cat.catId == anotherCat.catId
        }) else {
            return
        }
        
        cats.remove(at: index)
    }
    
    func fetchCats() -> [Cat] {
        return cats
    }
    
    
    // MARK: Private methods
    
    private func archiveObjectToFile(object: [Cat], file: String, atDirectory directory: String? = nil) {
        let filePath = createPathForFile(file: file, atDirectory: directory)
        
        _ = NSKeyedArchiver.archiveRootObject(object.encoded, toFile: filePath)
    }
    
    private func unarchiveObjectFromFile(file: String, atDirectory directory: String? = nil) -> AnyObject? {
        let filePath = createPathForFile(file: file, atDirectory: directory)
        
        let object = (NSKeyedUnarchiver.unarchiveObject(withFile: filePath) as? [Cat.Coding])?.decoded
        
        return object as AnyObject?
    }
    
    private func removeFileAtPath(path: String, atDirectory directory: String? = nil) {
        let fileManager = FileManager.default
        
        let filePath = createPathForFile(file: path, atDirectory: directory)
        
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch {
            
        }
    }
    
    private func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        guard let documentsDirectory = paths.first else {
            abort()
        }
        
        return documentsDirectory
    }
    
    private func createPathForFile(file: String, atDirectory directory: String? = nil) -> String {
        var filePath: String
        
        if let customDirectory = directory {
            filePath = customDirectory.appending(file)
        } else {
            filePath = getDocumentsDirectory().appending(file)
        }
        
        return filePath
    }
}

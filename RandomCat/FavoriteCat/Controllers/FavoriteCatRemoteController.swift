//
//  FavoriteCatRemoteController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import Foundation

protocol FavoriteCatRemoteControllerDelegate: class {
    func didFavoriteCat(_ cat: Cat)
    func didFailFavoritingCat()
    func didUnfavoriteCat(_ cat: Cat)
    func didFailUnfavoritingCat()
}

class FavoriteCatRemoteController {
    
    private let favoritePath = "/favourite?api_key=\(userId)&sub_id=\(userId)&image_id=%@"
    private let unfavoritePath = "/favourite?api_key=\(apiKey)&sub_id=\(userId)&image_id=%@&action=remove"
    private let remoteController = RemoteController()
    
    weak var delegate: FavoriteCatRemoteControllerDelegate?
    
    func markAsFavorite(_ cat: Cat) {
        let path = String.init(format: favoritePath, cat.catId)
        let urlString = remoteController.baseURL + path
        
        guard let url = URL(string: urlString) else {
            delegate?.didFailFavoritingCat()
            return
        }
        
        let request = URLRequest(url: url)
        
        remoteController.performNetworkRequest(request, completionHandler: { [weak self] (data, response) in
            self?.delegate?.didFavoriteCat(cat)
        }, failureHandler: { [weak self] (error) -> Void in
            self?.delegate?.didFailFavoritingCat()
        })
    }
    
    func unfavorite(_ cat: Cat) {
        let path = String.init(format: unfavoritePath, cat.catId)
        let urlString = remoteController.baseURL + path
        
        guard let url = URL(string: urlString) else {
            delegate?.didFailUnfavoritingCat()
            return
        }
        
        let request = URLRequest(url: url)
        
        remoteController.performNetworkRequest(request, completionHandler: { [weak self] (data, response) in
            self?.delegate?.didUnfavoriteCat(cat)
            }, failureHandler: { [weak self] (error) -> Void in
                self?.delegate?.didFailUnfavoritingCat()
        })
    }
}

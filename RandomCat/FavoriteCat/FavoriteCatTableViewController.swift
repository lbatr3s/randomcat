//
//  FavoriteCatTableViewController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

class FavoriteCatTableViewController: UITableViewController {
    
    private var cats = FavoriteCatDataController.sharedInstance.fetchCats() {
        didSet {
            tableView.reloadData()
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cats = FavoriteCatDataController.sharedInstance.fetchCats()
    }

    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cats.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavoriteCatTableViewCell

        let cat = cats[indexPath.row]
        
        cell.configure(cat: cat)

        return cell
    }
}

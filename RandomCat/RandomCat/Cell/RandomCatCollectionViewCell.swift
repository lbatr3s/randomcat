//
//  RandomCatCollectionViewCell.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

protocol RandomCatCollectionViewCellDeleagate: class {
    func didTapFavorite(cat: Cat)
    func didTapUnfavorite(cat: Cat)
}

class RandomCatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView! {
        didSet {
            indicatorView.activityIndicatorViewStyle = .gray
            indicatorView.hidesWhenStopped = true
            indicatorView.startAnimating()
        }
    }
    
    private var cat: Cat!
    
    static let identifier = "Cell"
    
    weak var delegate: RandomCatCollectionViewCellDeleagate?
    
    func configure(cat: Cat) {
        self.cat = cat
        
        let buttonTitle: String
        
        if cat.isFavorite {
            buttonTitle = "Unfavorite"
        } else {
            buttonTitle = "Favorite"
        }
        
        favButton.titleLabel?.text = buttonTitle
        
        guard let url = cat.url else {
            return
        }
        
        imageView.loadImage(fromURL: url, activityIndicator: indicatorView)
    }
    
    
    // MARK: Action methods
    
    @IBAction func didTapFavoriteButton(_ sender: UIButton) {
        if cat.isFavorite {
            delegate?.didTapUnfavorite(cat: cat)
        } else {
            delegate?.didTapFavorite(cat: cat)
        }
    }
}

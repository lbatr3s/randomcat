//
//  RandomCatCollectionViewController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

class RandomCatCollectionViewController: UICollectionViewController {
    
    private var randomCatController = RandomCatController()
    private var favoriteCatController = FavoriteCatController()
    
    private var cats: [Cat] = [] {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        randomCatController.delegate = self
        favoriteCatController.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        randomCatController.getCats()
    }
    
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cats.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RandomCatCollectionViewCell.identifier, for: indexPath) as! RandomCatCollectionViewCell
        cell.delegate = self
        
        let cat = cats[indexPath.row]
        cell.configure(cat: cat)
        cell.delegate = self
        
        return cell
    }
}


// MARK: RandomCatController delegate methods

extension RandomCatCollectionViewController: RandomCatControllerDelegate {
    
    func didGetCats(_ cats: [Cat]) {
        DispatchQueue.main.async {
            print(cats)
            self.cats = cats
        }
    }
    
    func didFailGettingCats() {
        
    }
}


// MARK: RandomCatCollectionViewCell delegate methods

extension RandomCatCollectionViewController: RandomCatCollectionViewCellDeleagate {
    
    func didTapFavorite(cat: Cat) {
        favoriteCatController.markAsFavorite(cat)
    }
    
    func didTapUnfavorite(cat: Cat) {
        favoriteCatController.unfavorite(cat)
    }
}


// MARK: FavoriteCatController delegate methods

extension RandomCatCollectionViewController: FavoriteCatControllerDelegate {
    
    func didFavoriteCat(_ cat: Cat) {
        guard let index = indexForCat(cat) else {
            return
        }
        
        DispatchQueue.main.async {
            self.updateCat(cat, atIndex: index, favorite: true)
        }
    }
    
    func didUnfavoriteCat(_ cat: Cat) {
        guard let index = indexForCat(cat) else {
            return
        }
        
        DispatchQueue.main.async {
            self.updateCat(cat, atIndex: index, favorite: false)
        }
    }
    
    func didFailFavoritingCat() {
        
    }
    
    func didFailUnfavoritingCat() {
        
    }
    
    
    // MARK: Private methods
    
    private func indexForCat(_ cat: Cat) -> Int? {
        return cats.index(where: {(anotherCat) -> Bool in
            return cat.catId == anotherCat.catId
        })
    }
    
    private func updateCat(_ cat: Cat, atIndex index: Int, favorite: Bool) {
        var mutableCat = cat
        
        if favorite {
            mutableCat.markAsFavorite()
            FavoriteCatDataController.sharedInstance.saveCat(mutableCat)
        } else {
            mutableCat.unfavorite()
            FavoriteCatDataController.sharedInstance.removeCat(mutableCat)
        }
        
        cats[index] = mutableCat
    }
}

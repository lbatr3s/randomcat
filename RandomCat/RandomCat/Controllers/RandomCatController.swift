//
//  RandomCatController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

protocol RandomCatControllerDelegate: class {
    
    func didGetCats(_ cats: [Cat])
    func didFailGettingCats()
}

class RandomCatController {
    
    private let remoteController: RandomCatRemoteController
    
    weak var delegate: RandomCatControllerDelegate?
    
    init() {
        remoteController = RandomCatRemoteController()
        remoteController.delegate = self
    }
    
    func getCats() {
        remoteController.getCats()
    }
}


// MARK: RandomCatRemoteController delegate methods

extension RandomCatController: RandomCatRemoteControllerDelegate {
    
    func didGetCats(controller: RandomCatRemoteController, cats: [Cat]) {
        delegate?.didGetCats(cats)
    }
    
    func didFailGettingCats() {
        delegate?.didFailGettingCats()
    }
}

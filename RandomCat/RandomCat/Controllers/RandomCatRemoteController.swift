//
//  RandomCatRemoteController.swift
//  RandomCat
//
//  Created by Lester Batres on 11/9/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import Foundation
import SWXMLHash

protocol RandomCatRemoteControllerDelegate: class {
    func didGetCats(controller: RandomCatRemoteController, cats: [Cat])
    func didFailGettingCats()
}

class RandomCatRemoteController {
    
    private let path = "/get?api_key=\(apiKey)&format=xml&results_per_page=20&type=png,jpg&sub_id=\(userId)"
    private let remoteController = RemoteController()
    
    weak var delegate: RandomCatRemoteControllerDelegate?
    
    lazy var url: URL? = {
        let urlString = remoteController.baseURL + path
        
        return URL(string: urlString)
    }()
    
    func getCats() {
        guard let url = url else {
            return
        }
        
        let request = URLRequest(url: url)
        
        remoteController.performNetworkRequest(request, completionHandler: { [weak self] (data, response) in
            guard let weakSelf = self else {
                return
            }
            
            do {
                let cats = try weakSelf.parseResponse(data: data)
                weakSelf.delegate?.didGetCats(controller: weakSelf, cats: cats)
            } catch {
                weakSelf.delegate?.didFailGettingCats()
            }
            
            }, failureHandler: { [weak self] (error) -> Void in
                self?.delegate?.didFailGettingCats()
        })
    }
    
    
    // MARK: Private methods
    
    private func parseResponse(data: Data) throws -> [Cat] {
        let xml = SWXMLHash.parse(data)
        let cats: [Cat] = try xml[XMLKey.response][XMLKey.data][XMLKey.images][XMLKey.image].value()
        
        return cats
    }
}
